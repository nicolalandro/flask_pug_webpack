from flask import Flask, render_template
from flask_webpack import Webpack
from werkzeug.serving import run_simple

webpack = Webpack()
app = Flask(__name__)
app.jinja_env.add_extension('pypugjs.ext.jinja.PyPugJSExtension')
app.config.update({'DEBUG': True, 'WEBPACK_MANIFEST_PATH': './build/manifest.json'})
webpack.init_app(app)


@app.route('/')
@app.route('/<name>')
def hello(name=None):
    return render_template('hello.pug', name=name)


if __name__ == "__main__":
    run_simple('localhost', 5000, app, use_reloader=True, use_debugger=True)
